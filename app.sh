#!/usr/bin/env sh

DC_RUN_ARGS="--rm"
DC_COMPOSE="docker compose -f docker-compose.yml -f docker-compose.production.yml -f docker-compose.frontend.yml --env-file .env"

if [ $# -gt 0 ]; then
  case $1 in
    "setup")
      set -x \
      && mkdir -p ./storage/framework \
      && chmod -R 777 ./storage \
      && mkdir -p ./backups \
      && mkdir -p ~/images \
      && $DC_COMPOSE run $DC_RUN_ARGS app php artisan migrate --force \
      && $DC_COMPOSE run $DC_RUN_ARGS app php artisan db:seed --force \
      && set +x
    ;;
    "deploy-backend")
      set -x \
      && cwd=$(pwd) \
      && cd ~/images \
      && image_name=$(ls -Art | grep wc-backend | tail -n 1) \
      && docker load < "$image_name" \
      && docker tag epalshin/wc-backend wc/backend \
      && cd "$cwd" \
      && $DC_COMPOSE up -d nginx websockets queue cron \
      && $DC_COMPOSE run $DC_RUN_ARGS app php artisan migrate --force \
      && set +x
    ;;
    "deploy-frontend")
      set -x \
      && cwd=$(pwd) \
      && cd ~/images \
      && image_name=$(ls -Art | grep wc-frontend | tail -n 1) \
      && docker load < "$image_name" \
      && cd "$cwd" \
      && $DC_COMPOSE up -d frontend \
      && set +x
    ;;
    "up")
      $DC_COMPOSE up -d redis frontend websockets queue cron
    ;;
    *)
      $DC_COMPOSE $@
    ;;
  esac
else
  $DC_COMPOSE ps
fi
