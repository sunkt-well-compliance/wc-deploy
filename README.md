# Deploy WC

Продуктовая среда для приложения.

Предполагается, что на сервере установлен docker и docker compose v2 и выше (см. [https://docs.docker.com/compose/cli-command/#install-on-linux](инструкцию по установке compose v2).

### Установка проекта
```shell
git clone git@bitbucket.org:sunkt-well-compliance/wc-deploy.git wc
cd wc
chmod +x deploy.sh
cp .env.production.example .env
vi .env # далее отредактировать в соответствии с параметрами сервера
./deploy.sh setup
```

### Подключение к Bitbucket Pipelines

На сервер нужно добавить ssh ключ от пайплайнов битбакета:

```shell
echo "key" >> ~/.ssh/authorized_keys
```

После этого не забыть добавить новый хост в known_hosts битбакета.

**В known_hosts нужно добавить ip-адрес сервера с указанием порта ssh: 8.8.8.8:5151. Но если порт 22 (стандартный ssh), его указывать не нужно! Явное указание приведет к тому, что пайплайны не будут работать**

Деплой осуществляется из битбакета (Run Pipeline -> Build And Deploy)
